const fs = require("fs");
function problem2() {
  fs.promises.readFile("./lipusm.txt", "utf-8").then((resolveFile) => {
    const upperCase = resolveFile.toUpperCase();
    fs.promises.writeFile("./file1", upperCase).then(() => {
      fs.promises.appendFile("./filename.txt", "file1").then(() => {
        fs.promises.readFile("./lipusm.txt", "utf-8").then((resolveFile1) => {
          const lowerCase = resolveFile1.toLowerCase().split(".");
          fs.promises.writeFile("./file2", lowerCase).then(() => {
            fs.promises.appendFile("./filename.txt", "&file2").then(() => {
              fs.promises.readFile("./file1", "utf-8").then((resolveFile2) => {
                fs.promises
                  .readFile("./file2", "utf-8")
                  .then((resolveFile3) => {
                    let sortedData = (resolveFile2 + resolveFile3)
                      .split("")
                      .sort()
                      .join("");
                    fs.promises.writeFile("./file3", sortedData).then(() => {
                      fs.promises
                        .appendFile("./filename.txt", "&file3")
                        .then(() => {
                          fs.promises
                            .readFile("./filename.txt", "utf-8")
                            .then((resolveFile4) => {
                              const arrayOfFileName = resolveFile4.split("&");
                              for (let i = 0; i < arrayOfFileName.length; i++) {
                                fs.promises
                                  .unlink(`./${arrayOfFileName[i]}`)
                                  .then((res) => {
                                    console.log("Files Deleted");
                                  })
                                  .catch((err) => {
                                    console.log("err");
                                  });
                              }
                            });
                        });
                    });
                  });
              });
            });
          });
        });
      });
    });
  });
}
module.exports = problem2;
