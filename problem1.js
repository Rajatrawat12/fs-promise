const fs = require("fs");
const path = require("path");
function problem1() {
  fs.promises.mkdir(path.join(__dirname, "intro")).then((resolveFile) => {
    fs.promises.writeFile("./name.json", " {Name : Rajat}").then(() => {
      fs.promises
        .unlink("./name.json")
        .then(() => {
          console.log("file deleted");
        })
        .catch((err) => {
          console.log("err");
        });
    });
  });
}
module.exports = problem1;
